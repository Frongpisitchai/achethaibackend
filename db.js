const {Client} = require('pg');

const client = new Client({
    user: 'Master',
    host: 'achethai.c5e8vnjeufbz.us-east-2.rds.amazonaws.com',
    database: 'AchethaiDB',
    password: 'Gotom00n',
    port: 5432,
    //connectionTimeoutMillis: 30000
  })
  client.connect(err => {
    if (err) {
      console.error('connection error', err.stack)
    } else {
      console.log('connected')
    }
  })
const allinsectarium = (request, response) => {
    const id = parseInt(request.params.id)

    client.query(`select insectarium_id,insectarium_status,insectarium_lossc,insectarium_name,insectarium_amm from insectarium where farm_id = $1 order by insectarium_id ASC` ,[id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}
const insectarium = (request, response) => {
    const id = parseInt(request.params.id)

    client.query(`select insectarium_id,insectarium_status,insectarium_lossc,insectarium_name,insectarium_amm from insectarium where insectarium_id = $1` ,[id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}
const allpound = (request, response) => {
    const id = parseInt(request.params.id)

    client.query(`select pound_id,pound_status,pound_name,pound_stench,pound_dead,pound_foodc,pound_waterc,pound_amm,pound_duration,pound_enemies from pound where insectarium_id = $1 order by pound_id ASC` ,[id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
    console.log("all good")
  })
}
const pound = (request, response) => {
    const id = parseInt(request.params.id)

    client.query(`select pound_id,pound_status,pound_name,pound_stench,pound_dead,pound_foodc,pound_waterc,pound_amm,pound_duration,pound_enemies from pound where pound_id = $1 order by pound_id ASC` ,[id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}
const countcrickettype = (request, response) => {

    client.query(`select count(insecttype_id)count,insecttype_id from pound group by insecttype_id` , (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}
const updateamm = (request, response) => {
    const id = parseInt(request.params.id)
    const amm = parseInt(request.body.amm)
    //const ammd = parseFloat(amm)
    console.log("id",id)
    console.log("amm",amm)
    client.query('update pound set pound_amm=$1 where pound_id =$2',
    [amm, id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).send(`Amm modified with pound_ID: ${id}`)
      }
    )
}
const updatefood = (request, response) => {
    const id = parseInt(request.params.id)
    const food = request.body.food
    console.log(food)
    client.query('update pound set pound_foodc=(pound_foodc+$1) where pound_id =$2',
    [food, id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).send(`Food modified with pound_ID: ${id}`)
      }
    )
}
const updatewater = (request, response) => {
    const id = parseInt(request.params.id)
    const water = request.body.water
    console.log(water)
    client.query('update pound set pound_waterc=(pound_waterc+$1) where pound_id =$2',
    [water, id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).send(`Water modified with pound_ID: ${id}`)
      }
    )
}
const updatepoundstatus = (request, response) => {
    const id = parseInt(request.params.id)
    const status = request.body.status
    const smell = request.body.smell
    const dead = request.body.dead
    const enemy = request.body.enemy
    console.log(id)
    console.log("status",status)
    console.log("smell",smell)
    console.log("dead",dead)

    //console.log(water)
    client.query('update pound set pound_status=$1,pound_stench=$2,pound_dead=$3,pound_enemies=$4 where pound_id =$5',
    [status,smell,dead,enemy, id],
      (error, results) => {
        if (error) {
          throw error
        }
        console.log(`Pound_Status modified with pound_ID: ${id}`)
        response.status(200).send(`Pound_Status modified with pound_ID: ${id}`)
      }
    )
}
const updateinsectariumstatus = (request, response) => {
  const id = parseInt(request.params.id)
  const status = request.body.status
  console.log(id)
  console.log(status)
  //console.log(water)
  client.query('update insectarium set insectarium_status =$1  where insectarium_id=$2 ',
  [status,id],
    (error, results) => {
      if (error) {
        throw error
      }
      console.log(`Pound_Status modified with pound_ID: ${id}`)
      response.status(200).send(`Pound_Status modified with pound_ID: ${id}`)
    }
  )
}
const allenemies = (request, response) => {
  //console.log(water)
  client.query('select * from enemies',
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    }
  )
}
const updateenemies = (request, response) => {
  const id = parseInt(request.params.id)
  const eid = request.body.eid
  const status = 'ไม่ปกติ'
  console.log(id)
  console.log(eid)
  //console.log(water)
  client.query('update pound set enemies_id =$1,pound_enemies=$2  where pound_id=$3',
  [eid,status,id],
    (error, results) => {
      if (error) {
        throw error
      }
      console.log(`Enemies_id modified with pound_ID: ${id}`)
      response.status(200).send(`Enemies_id modified with pound_ID: ${id}`)
    }
  )
}
const updateenemiesnormal = (request, response) => {
  const id = parseInt(request.params.id)
  const status = 'ปกติ'
  console.log(id)
  //console.log(water)
  client.query('update pound set pound_enemies=$1  where pound_id=$2',
  [status,id],
    (error, results) => {
      if (error) {
        throw error
      }
      console.log(`Enemies_id modified with pound_ID: ${id}`)
      response.status(200).send(`Enemies_id modified with pound_ID: ${id}`)
    }
  )
}
const assignedorder = async (request, response) => {
  const data = request.params.data
  client.query(`select * from assigned where assigned_date = $1` ,[data], async (error, results) => {
  if (error) {
    throw error
  }
  const big =results.rows
  let temp = []
  let tempsub = []
  const a =big.length
  //console.log("big len",a)
  for (i=0;i<a;i++){
    const aid = results.rows[i].assigned_id
    //console.log("aid",aid)
    await client.query(`select pound_id from poundlist where assigned_id = $1` ,[aid], (error, resultssub) => {
    if (error) {
      throw error
    }
    const b = resultssub.rows.length
    let tempsub = []
    console.log("B",b)
    for(i =0; i<b;i++){
      tempsub.push(resultssub.rows[i].pound_id)
    }
    console.log(tempsub);
    //console.log("subQ",typeof resultssub.rows)
    //temp.push( resultssub.rows)
    //console.log("bigbigbigBIG",big)
    })
    // console.log("temp",temp)
    // console.log("tempTYPE",typeof temp[i])
    //big[i].pounds=temp[i]
    //console.log("BIG I", big[i])
    // console.log("I",i)
    // console.log("END LOOP")
  } 
  console.log("TEMPSUB OUTLOOP",tempsub);
  for(i=0;i<a;i++){
    //console.log("temp",temp)
    //console.log("tempI",temp[i])
    big[i].pounds=temp[i]
    //console.log(tempsub);

    //console.log("BIG I", big[i])
  }
  console.log(big)  
  //console.log(results.rows[0].assigned_id)
  //results.rows.push(a)
  response.status(200).json(results.rows)
})
}
const poundlistass = (request, response) => {
  //const data = request.body.data
  const id = parseInt(request.params.id)
  console.log(typeof data)
  client.query(`select pound_id from poundlist where assigned_id = $1` ,[id], (error, results) => {
  if (error) {
    throw error
  }
  console.log("poundlist is OK",results)
  const a = results.rows.length
  let temp = [];
  for(i =0; i<a;i++){

    temp.push(results.rows[i].pound_id)

  }
  console.log(temp)
  response.status(200).json(temp)
})
}
const addact = (request, response) => {
  const actname = request.body.actname
  const mantask = request.body.mantask
  const worktask = request.body.worktask
  const typeInput = request.body.typeInput
  console.log(request)
  client.query('insert into activity(activity_name) values($1);',[actname],
    (error, results) => {
      if (error) {
        throw error
      } 
      console.log(`Add act fin`)
      response.status(200).send(`Add act fin`)
    }
  )
  const temp = mantask.length
  for(i = 0;i<temp;i++){
    var b = mantask.shift();
    console.log(b);
    client.query('insert into managertask(managertask_name,activity_id) values($1,(select activity_id from activity order by activity_id DESC LIMIT 1));',
    [b],
      (error, results) => {
        if (error) {
          throw error
        }
        console.log(`Add mantask fin`)
        //response.status(200).send(`Add mantask fin`)
      }
    )
  }

  const tempw = worktask.length
  for(i = 0;i<tempw;i++){
    var b = worktask.shift();
    var c = typeInput.shift();
    console.log(b);
    client.query('insert into workertask( workertask_name,workertask_type,activity_id) values($1,$2,(select activity_id from activity order by activity_id DESC LIMIT 1));',
    [b,c],
      (error, results) => {
        if (error) {
          throw error
        }
        console.log(`Add worktask fin`)
        //response.status(200).send(`Add mantask fin`)
      }
    )
  }

}
const allegg = (request, response) => { 
  const id = parseInt(request.params.id)

  client.query(`select * from egg where farm_id = $1 order by egg_id ASC;` ,[id], (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
  console.log("all good")
})
}
const allbox = (request, response) => {
  const id = parseInt(request.params.id)
  client.query(`select * from box where egg_id = $1 order by box_id ASC;` ,[id], (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
  console.log("all good")
})
}
const egg = (request, response) => { 
  const id = parseInt(request.params.id)

  client.query(`select * from egg where egg_id = $1 order by egg_id ASC;` ,[id], (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
  console.log("all good")
})
}
const user = (request, response) => {
  const username = request.body.username
  const pass = request.body.pass


  client.query(`select * from worker where  worker_username = $1 and worker_tel = $2`,[username,pass],(error, results) => {
  if (error) {
    throw error
  }
  if (results.rowCount==0){
               return response.json({
                 'code':404 , "Message": "ID/PASS not match"
               })
             }
             else{
               return response.json({
                 'code' :200 , "Message": "Success","Data": results.rows
               })
             }
  //response.status(200).json(results.rows)
  //console.log("all good")
})
}
const updateeggstatus = (request, response) => {
  const id = parseInt(request.params.id)
  const status = request.body.status
  console.log(id)
  console.log(status)
  //console.log(water)
  client.query('update egg set egg_lossc =$1  where egg_id=$2',
  [status,id],
    (error, results) => {
      if (error) {
        throw error
      }
      console.log(`Pound_Status modified with pound_ID: ${id}`)
      response.status(200).send(`Pound_Status modified with pound_ID: ${id}`)
    }
  )
}
const updateboxstatus = (request, response) => {
  const id = parseInt(request.params.id)
  const enemy = request.body.enemy
  const bac = request.body.bac
  const coconut = request.body.coconut
  const enemyid = request.body.enemyid
  const green = request.body.green
  const white = request.body.white
  const status = request.body.status

  // console.log(id)
  // console.log("status",status)
  // console.log("smell",smell)
  // console.log("dead",dead)
  //console.log(water)
  client.query('update box set box_enemies=$1,box_bac=$2,box_coconut=$3,enemies_id=$4,box_greenbac=$5,box_whitebac=$6,box_status=$7 where box_id =$8',
  [enemy,bac,coconut,enemyid,green,white,status,id],
    (error, results) => {
      if (error) {
        throw error 
      }
      console.log(`BOX modified with BOXID: ${id}`)
      response.status(200).send(`BOX modified with BOXID: ${id}`)
    }
  )
}
const notidashp = (request, response) => { 
  const id = parseInt(request.params.id)
  client.query(`  ` ,[id], (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
  //console.log(sum)
})
}
const notidashi = (request, response) => { 
  const id = parseInt(request.params.id)
  client.query(`select * from insectarium where farm_id = $1 AND(insectarium_status = 'ไม่ปกติ' OR insectarium_lossc = 'ไม่ปกติ')` ,[id], (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
  //console.log(sum)
})
}
const workerform = (request, response) => {
  const id = parseInt(request.params.id)
  const cloth = request.body.cloth
  const nail = request.body.nail
  const shoes = request.body.shoes
  const sick = request.body.sick
  const sicksymtom = request.body.sicksymtom
  const smell = request.body.smell
  const wash = request.body.wash
  const wear = request.body.wear

  console.log(cloth)
  client.query('insert into ph01(worker_id,cloth,nail,shoes,sick,sicksymtom,smell,wash,wear,ph01_time) values($1,$2,$3,$4,$5,$6,$7,$8,$9,now())',
  [id,cloth,nail,shoes,sick,sicksymtom,smell,wash,wear],
    (error, results) => {
      if (error) {
        throw error
      }
      console.log(`insert ph01form fin`)
      response.status(200).send(`insert ph01form fin`)
    }
  )
}
const addasigned = (request, response) => {
  const id = parseInt(request.params.id)
  client.query(`SELECT *
  FROM activity  
  full JOIN workertask
  ON ( workertask.activity_id = activity.activity_id )
  full JOIN managertask
  ON ( managertask.activity_id = activity.activity_id ) 
  WHERE activity.activity_id = $1;  `,[id] , (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
})
}
const addwtask = (request, response) => {
  const id = parseInt(request.params.id)
  client.query(`select * from workertask where activity_id =$1`,[id] , (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
})
}
const addmtask = (request, response) => {
  const id = parseInt(request.params.id)
  client.query(`select * from managertask where activity_id =$1`,[id] , (error, results) => {
  if (error) {
    throw error
  } 
  response.status(200).json(results.rows)
})
}
const worker = (request, response) => { 
  const id = parseInt(request.params.id)
  client.query(`select * from worker where farm_id = $1 AND worker_status=0;` ,[id], (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
  console.log("all good")
})
}
const updateassign = (request, response) => {
  const adminWork = request.body.adminWorks
  const endTime = request.body.endTime
  const insectarium = parseInt(request.body.insectarium)
  const jobDate = request.body.jobDate
  const jobFrequency = parseInt(request.body.jobFrequency)
  const note = request.body.note
  const pound = request.body.pond
  const selectedJobName = parseInt(request.body.selectedJobName)
  const startTime = request.body.startTime
  const worker = request.body.worker
  const adminWorkIds = request.body.adminWorkIds
  console.log(adminWork)
  console.log(endTime)
  console.log(jobDate)
  console.log(jobFrequency)
  console.log(note)
  console.log(startTime)
  console.log(worker)
  client.query(`insert into assigned(assigned_note,assigned_type,assigned_date,assigned_timestart,assigned_timefin,assigned_timetype,activity_id) values ($1,$2,$3,$4,$5,$6,$7);`,[note,jobFrequency,jobDate,startTime,endTime,jobFrequency,selectedJobName] ,
  (error, results) => {
    if (error) {
      console.log(error)
    }
    console.log(`Add assigned fin`)
    response.status(200).send(`Add mantask fin`)
  }
)
const tempwork = worker.length
for(i = 0;i<tempwork;i++){
  var c = worker.shift();

  console.log("workerNoarray",c);
  client.query(`update worker set worker_status = 1 where worker_id = $1`,[c] ,
    (error, results) => {
      if (error) {
        console.log(error)
      }
      console.log(`Add worker fin`)
      //response.status(200).send(`Add mantask fin`)
    }
  ) 
}
}
const updateassign2 = (request, response) => { 
  const adminWork = request.body.adminWorks
  const endTime = request.body.endTime
  const insectarium = parseInt(request.body.insectarium)
  const jobDate = request.body.jobDate
  const jobFrequency = parseInt(request.body.jobFrequency)
  const note = request.body.note
  const pound = request.body.pond
  const selectedJobName = parseInt(request.body.selectedJobName)
  const startTime = request.body.startTime
  const worker = request.body.worker
  const adminWorkIds = request.body.adminWorkIds
  console.log("adminWork",adminWork)
  console.log("insectarium",insectarium)
  console.log("pound",pound)
  console.log("adminWorkIds",adminWorkIds)

  const temp = pound.length
  for(i = 0;i<temp;i++){
    var b = pound.shift();
    console.log("poundNoarray",b);
    client.query(`insert into poundlist(assigned_id,pound_id,insectarium_id) values((select assigned_id from assigned order by assigned_id DESC LIMIT 1),$1,$2)`,[b,insectarium] ,
      (error, results) => {
        if (error) {
          console.log(error)
        }
        console.log(`Add poundlist fin`)
        //response.status(200).send(`Add mantask fin`)
      }
    )
  }
const tempad = adminWork.length
for(i = 0;i<tempad;i++){
  var d = adminWork.shift();
  var e = adminWorkIds.shift();
  console.log("adminWorkNoarray",d);
  console.log("adminWorkIdsNoarray",d);
  client.query(`insert into mantaskval(mantaskval_val,assigned_id,managertask_id) values($1,(select assigned_id from assigned order by assigned_id DESC LIMIT 1),$2);`,[d,e] ,
    (error, results) => {
      if (error) {
        console.log(error)
      }
      console.log(`Add managertask fin`)
      //response.status(200).send(`Add mantask fin`)
    }
  )
}

const tempwk = worker.length
for(i = 0;i<tempwk;i++){
  var d = worker.shift();
  console.log("เข้าป้ะพ",d);

  console.log("workerNoarray",d);
  client.query(`insert into workerlist(worker_id,assigned_id) values($1,(select assigned_id from assigned order by assigned_id DESC LIMIT 1));`,[d] ,
    (error, results) => {
      if (error) {
        console.log(error)
      }
      console.log(`Add workerlist fin`)
      //response.status(200).send(`Add mantask fin`)
    }
  )
}



}
const allactivity = (request, response) => {
  client.query(`select * from activity` , (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
})
}
const allact = (request, response) => {//ส่ง date มา --> ได้ assigned_id
  const data = request.params.data
  client.query(`select assigned_id,activity_id,assigned_timestart,assigned_timefin,assigned_status from assigned where assigned_date = $1`,[data] , (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
})
}
//3 อันนี้นะฟร้อง

// const allact2 = (request, response) => {
//   const id = parseInt(request.params.id)
//   const aid = parseInt(request.params.aid)

//   let allres = [];
//   client.query(`select * from workerlist where assigned_id = $1` ,[id], (error, results) => {
//   if (error) {
//     throw error
//   }
//   allres.push(results.rows)
//   //response.status(200).json(results.rows)
// })
// client.query(`select * from poundlist where assigned_id = $1` ,[id], (error, results) => {
//   if (error) {
//     throw error
//   }
//   //console.log(allres)
//   allres.push(results.rows)
//   console.log(allres)
//   //response.status(200).json(allres)
// })
// client.query(`select * from activity where activity_id = $1` ,[aid], (error, results) => {
//   if (error) {
//     throw error
//   }
//   console.log(allres)
//   allres.push(results.rows)
//   console.log(allres)
//   response.status(200).json(allres)
// })
// }
const allact21 = (request, response) => { //ส่ง assigned_id มา --> ได้ worker_id
  const id = parseInt(request.params.id)
  client.query(`select * from workerlist where assigned_id = $1`,[id] , (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
})
}
const allact22 = (request, response) => {//ส่ง assigned_id มา --> ได้ poundlist
  const id = parseInt(request.params.id)
  client.query(`select * from poundlist where assigned_id = $1`,[id] , (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
})
}
const allact23 = (request, response) => {//ส่ง assigned_id มา --> ได้ activity_name
  const id = parseInt(request.params.id)
  client.query(`select * from activity where activity_id = $1`,[id] , (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
})
}
const allact3 = (request, response) => {//ส่ง worker_id มา --> ได้ worker_name
  const id = parseInt(request.params.id)
  client.query(`select worker_name from worker where worker_id =$1`,[id] , (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
})
}
const oneact = (request, response) => {//ส่ง assigned_id มา (หน้า JOBdetail)
  const id = parseInt(request.params.id)
  client.query(`select * from assigned where assigned_id = $1`,[id] , (error, results) => {
  if (error) {
    throw error
  }
  response.status(200).json(results.rows)
})
}




module.exports = {
    allinsectarium,
    insectarium,
    allpound,
    countcrickettype,
    pound,
    updateamm,
    updatefood,
    updatewater,
    updatepoundstatus,
    updateinsectariumstatus,
    allenemies,
    updateenemies,
    assignedorder,
    updateenemiesnormal,
    poundlistass,
    allegg,
    allbox,
    user,
    notidashp,
    notidashi,
    updateeggstatus,
    updateboxstatus,
    egg,
    workerform,
    addact,
    allact,
    addwtask,
    addmtask,
    worker,
    updateassign,
    updateassign2,
    //allact2,
    allact3,
    oneact,
    allactivity,
    allact21,
    allact22,
    allact23
  }