const express = require('express')
const app = express()
const bodyParser = require('body-parser')
var cors = require('cors')
app.use(cors(JSON))
app.use(bodyParser.json())
app.set('port', (process.env.PORT || 4000))
const db = require('./db')

var temppound = {} ;
var humipound = {} ;

var tempinsect = {} ;
var tempinsect = {} ;
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
)
// var url = new URL('https://example.com?id=5');
// var params = new URLSearchParams(url.search);
// console.log(params)
// const a = ["a","b","c ","d","e"]
// const temp = a.length
// console.log(temp.length)
// for(i = 0;i<temp;i++){
//   var b = a.shift();
//   console.log("b",b);
//   console.log("a",a);
//   //console.log("len",temp.length);
// }
app.get('/allinsectarium/:id', db.allinsectarium) //โรงเลี้ยงทั้งหมดในfarmที่กำหนด (หน้าdashboard)
app.get('/insectarium/:id', db.insectarium) //โรงเลี้ยงที่กำหนด (หน้าโรงเลี้ยง)
app.get('/allpound/:id', db.allpound) //บ่อเลี้ยงทั้งหมดในโรงเลี้ยงที่กำหนด (หน้าโรงเลี้ยง)
app.get('/pound/:id', db.pound) //บ่อเลี้ยงที่กำหนด (หน้าบ่อเลี้ยง)
app.get('/countcrickettype', db.countcrickettype) //จำนวนบ่อที่เลี้ยงพันธ์จิ้งหลีดที่กำหนด (หน้าdashboard)
app.post('/updateamm/:id',db.updateamm)
app.put('/updatefood/:id',db.updatefood)
app.put('/updatewater/:id',db.updatewater)
app.post('/updatepoundstatus/:id',db.updatepoundstatus)
app.post('/updateinsectariumstatus/:id',db.updateinsectariumstatus)
app.get('/allenemies',db.allenemies)
app.post('/updateenemies/:id',db.updateenemies)
app.get('/assignedorder/:data',db.assignedorder)
app.post('/updateenemiesnormal/:id',db.updateenemiesnormal)
app.get('/poundlistass/:id',db.poundlistass)
app.get('/allegg/:id',db.allegg)
app.get('/allbox/:id',db.allbox)
app.post('/user',db.user)
app.get('/notidashp/:id',db.notidashp)
app.get('/notidashi/:id',db.notidashi)
app.post('/updateeggstatus/:id',db.updateeggstatus)
app.post('/updateboxstatus/:id',db.updateboxstatus)
app.get('/egg/:id',db.egg)
app.post('/workerform/:id',db.workerform)
app.post('/addact',db.addact)
app.get('/addwtask/:id',db.addwtask)
app.get('/addmtask/:id',db.addmtask)
app.get('/worker/:id',db.worker)
app.post('/updateassign',db.updateassign)
app.post('/updateassign2',db.updateassign2)

//3 อันนี้นะฟร้อง
app.get('/allact/:data',db.allact)//ส่ง date มา --> ได้ assigned_id
//app.get('/allact2/:id/:aid',db.allact2)
app.get('/allact3/:id',db.allact3)//ส่ง worker_id มา --> ได้ worker_name
app.get('/oneact/:id',db.oneact)//ส่ง assigned_id มา (หน้า JOBdetail)
app.get('/allactivity',db.allactivity)
app.get('/allact21/:id',db.allact21)//ส่ง assigned_id มา --> ได้ worker_id
app.get('/allact22/:id',db.allact22)//ส่ง assigned_id มา --> ได้ poundlist
app.get('/allact23/:id',db.allact23)//ส่ง assigned_id มา --> ได้ activity_name


//app.put('/updatesmell/:id',db.updatesmell)
// app.post('/test', (_req,res) => {
//   res.json({ info: 'Node.js, Express, and Postgres API' })
// })

app.post('/api/temppound', function (req, res) {
	//console.log(JSON.stringify(req.body));
  temppound = JSON.stringify(req.body);
  //console.log(temppound);
  res.send(JSON.stringify(req.body));
})
app.get('/api/sendtemppound', function (req, res) {
  res.send(temppound);
})
app.post('/api/humipound', function (req, res) {
	//console.log(JSON.stringify(req.body));
  humipound = JSON.stringify(req.body);


  //console.log(humipound);
  res.send(JSON.stringify(req.body));
})
app.get('/api/sendhumipound', function (req, res) {
  res.send(humipound);
})
app.post('/api/tempinsect', function (req, res) {
	//console.log(JSON.stringify(req.body));
  tempinsect = JSON.stringify(req.body);
  //console.log(tempinsect);
  res.send(JSON.stringify(req.body));
})
app.get('/api/sendtempinsect', function (req, res) {
  res.send(tempinsect);
})
app.post('/api/humiinsect', function (req, res) {
	//console.log(JSON.stringify(req.body));
  humiinsect = JSON.stringify(req.body);
  //console.log(tempinsect);
  res.send(JSON.stringify(req.body));
})
app.get('/api/sendhumiinsect', function (req, res) {
  res.send(humiinsect);
})

//console.log("test",temp)
// app.post('/api/hum', function (req, res) {
// 	console.log(req.query.hum)
//   res.send('success : ' + res.query.hum)
// })

app.listen(4000, function () {
  console.log('run at port', 4000)
})

